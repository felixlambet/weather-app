<?php

namespace App\Http\Controllers;

use App\Weather;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Search;


class WeatherController extends Controller
{
    public function index(Request $request)
    {        
        try {
            $this->client = new \GuzzleHttp\Client();
            $this->searchList = Search::all();
            if($request->type == 'location'){
                $paramsWoeid = ['query' => $request->location];
                $param = $request->location;
                $url = 'https://www.metaweather.com/api/location/search/?'.http_build_query($paramsWoeid);
            }elseif($request->type == 'latlong'){
                $paramsWoeid = ['lattlong' => $request->lat.','.$request->long];
                $param = $request->lat.','.$request->long;
                $url = 'https://www.metaweather.com/api/location/search/?'.http_build_query($paramsWoeid);
            }else{
                return view('weather.info', get_object_vars($this))->with(['error' => 'Bad parameters']);
            }
            $requestWoeid = $this->client->get($url);      
        } catch (\Exception $e) {
            return view('weather.info')->with(['error' => 'Cannot connect to API Server', 'errorMessage' => $e->getMessage(), 'url' => $url]);  
        }
        $storeData = ['parameters' => $param, 'type' => $request->type];
        $responseWoeid = json_decode($requestWoeid->getBody()->getContents());        
        if(!empty($responseWoeid)){
            $locationName = strtoupper($responseWoeid[0]->title);
            $locationType = strtoupper($responseWoeid[0]->location_type);
            $this->locationName = $locationName;
            $woeid = $responseWoeid[0]->woeid;
            Search::updateOrCreate(['woeid' => $woeid], ['name' => $locationName]);
            $this->createWeatherInfo($woeid,$locationName,$locationType,$storeData);
        }
        return view('weather.info', get_object_vars($this))->with('status', 'Connected to API Server');    
    }

    public function createWeatherInfo($woeid, $locationName = '', $locationType = '', $storeData = [])
    {
        $date = Carbon::now();
        $cardHtml = '';
        for ($i=0; $i < 6 ; $i++) { 
            $dateParam = $date->format('Y/m/d');
            $request = $this->client->get("https://www.metaweather.com/api/location/$woeid/$dateParam");
            $response = $request->getBody()->getContents();
            $dayName = $date->format('l');
            $dataAPI = isset(json_decode($response)[0]) ? json_decode($response)[0] : 'No Data';
            if($i==0){
                $todayHtml = view('weather.today', compact('dayName', 'dataAPI','locationName', 'locationType'))->render();
            }else{
                $cardHtml .= view('weather.card', compact('dayName', 'dataAPI'))->render();    
            }
            $date = $date->addDay();
        }
        $this->cardHtml = $cardHtml;
        $this->todayHtml = $todayHtml;  
    }
}
