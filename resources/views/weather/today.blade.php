<div class="px-2">
	<div class="card bg-whites">
		<div class="card-header text-center h3">
			{{ $dayName }} , {{ $locationName }}
		</div>
		<div class="card-body mx-3 pb-1">
			@if ($dataAPI == 'No Data')
				No Data
			@else
				<center>
					<img src="https://www.metaweather.com/static/img/weather/png/64/{{$dataAPI->weather_state_abbr}}.png" width="auto" height="140px">
				</center>
				<div class="row font-weight-bold mt-4">
					<p class="px-2">Max : {{number_format($dataAPI->max_temp,2)}}°C</p>
					<p class="px-2">Min : {{number_format($dataAPI->min_temp,2)}}°C</p>
					<p class="px-2"><dd class="wind">
						<span class="dir dir-{{strtolower($dataAPI->wind_direction_compass)}}"></span>
						{{number_format($dataAPI->wind_speed,2)}}mph
					</dd></p>
				</div>
			@endif	
		</div>
	</div>
</div>	