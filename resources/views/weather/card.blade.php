<div class="w-20 px-2">
	<div class="card">
		<div class="card-header">
			{{ $dayName }}
		</div>
		<div class="card-body">
			@if ($dataAPI == 'No Data')
				No Data
			@else
				<div class="row no-gutters">
					<div class="col">
						<img src="https://www.metaweather.com/static/img/weather/png/64/{{$dataAPI->weather_state_abbr}}.png">
					</div>
					<div class="col small">
						<p>Max : {{number_format($dataAPI->max_temp,2)}}°C</p>
						<p>Min : {{number_format($dataAPI->min_temp,2)}}°C</p>
						<p><dd class="wind">
							<span class="dir dir-{{strtolower($dataAPI->wind_direction_compass)}}"></span>
							{{number_format($dataAPI->wind_speed,2)}}mph
						</dd></p>
					</div>
				</div>
			@endif	
		</div>	
	</div>
</div>	