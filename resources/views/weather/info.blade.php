<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/custom.css') }}">
</head>
<body class="bg-body">
	<div class="container-fluid">
		<center class="row justify-content-md-center py-3">
			<form class="form-inline" action="{{ action('WeatherController@index') }}" method="GET">
				<div class="form-group">
					<input type="text" name="location" placeholder="Search" style="width: auto; background-color: #ffffff54;" class="form-control text-center">
					<input type="hidden" name="type" value="location">
					<button type="submit" class="btn btn-primary mx-2 expand-sc">Submit</button>
					<button type="button" class="btn btn-primary mx-2" id="currentLocation">Current Location</button>
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
					  Saved Location
					</button>
				</div>
			</form>
		</center>
		<div class="row justify-content-md-center py-3">
			@if (isset($error))
			<div class="col" style="text-align: center;">
				<p>{{$error}}</p>
				<p>{{isset($errorMessage) ? $errorMessage : ''}}</p>
				<p>{{isset($url) ? $url : ''}}</p>
			</div>
			@else
				@if (isset($todayHtml))
					{!! $todayHtml !!}
				@else
					Location Not Found
				@endif
			@endif
		</div>
		<div class="row mx-0 my-2">
			@if (isset($cardHtml))
				{!! $cardHtml !!}
			@endif	
		</div>
	</div>


	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">List of Saved Location</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<table class="table">
						<thead>
							<tr>
								<th>ID</th>
								<th>Name</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($searchList as $search)
								<tr>
									<td>{{$search->woeid}}</td>
									<td>{{$search->name}}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

<script>
	$(document).ready(function(){
		$('#currentLocation').click(function(){
			if (navigator.geolocation) {
		        navigator.geolocation.getCurrentPosition(showPosition);
		    } else {
		        x.innerHTML = "Geolocation is not supported by this browser.";
		    }
		});

		function showPosition(position) {
			var url = new URL(location.toString().replace(location.search, ""));
			url.searchParams.set('lat',position.coords.latitude);
			url.searchParams.set('long',position.coords.longitude);
			url.searchParams.set('type','latlong');
			window.location.href = url.href;
		}
	});

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

</script>
</html>
